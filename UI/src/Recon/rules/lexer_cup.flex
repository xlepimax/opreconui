package oprecon;

/*
  UNIBE
  Grupo #7
  Teoria de compiladores.

  It's not that we're so smart, it's just that we stay with problems longer.
*/

import java_cup.runtime.Symbol;
%%
%class LexerCup
%type java_cup.runtime.Symbol

%cup
%full
%line
%char

D=[0-9]
SEPARATOR=[\ \t\r\n]

%{
    private Symbol symbol(int type, Object value){
        return new Symbol(type, yyline, yycolumn, value);
    }
%}
%%

/* Empty space? */
{SEPARATOR} { /* Nothing to do here... */ }

/* Math Operators rule */

( "+" ) {return new Symbol(sym.ADD, yychar, yyline, yytext());}
( "-" ) {return new Symbol(sym.MINUS, yychar, yyline, yytext());}
( "*" ) {return new Symbol(sym.TIMES, yychar, yyline, yytext());}
( "/" ) {return new Symbol(sym.DIVIDE, yychar, yyline, yytext());}
( "^" ) {return new Symbol(sym.POWER, yychar, yyline, yytext());}
( "(" ) {return new Symbol(sym.LEFT_P, yychar, yyline, yytext());}
( ")" ) {return new Symbol(sym.RIGHT_P, yychar, yyline, yytext());}

/* Numero */
("("{D}+")")|{D}+ {return new Symbol(sym.NUMBER, yychar, yyline, yytext());}

/* Error de analisis */
 . {return new Symbol(sym.UNEXPECTED, yychar, yyline, yytext());}
