/*
  UNIBE
  Grupo #7
  Teoria de compiladores.

  It's not that we're so smart, it's just that we stay with problems longer.
*/

package Recon;

public enum Tokens
{
	UNEXPECTED,
	SEPARATOR,
	NUMBER,
	LEFT_P,
	RIGHT_P,
	ADD,
	MINUS,
	DIVIDE,
	POWER,
	TIMES
}
