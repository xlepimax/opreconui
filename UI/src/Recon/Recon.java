/*
  UNIBE
  Grupo #7
  Teoria de compiladores.

  It's not that we're so smart, it's just that we stay with problems longer.
 */
package Recon;

import java.io.IOException;
import java.util.ArrayList;
import java.io.StringReader;
import java_cup.runtime.Symbol;

public class Recon {
    public static ArrayList<String> analyzeTokens(String input) throws IOException{
        ArrayList<String> tokens = new ArrayList<>();
        for (int i = 0; i < input.length(); i++) {
            Lexer lexer = new Lexer(new StringReader(String.valueOf(input.charAt(i))));
            Tokens token = lexer.yylex();
            tokens.add(token.toString());
        }
        return tokens;
    }

    public static boolean isValid(String input) throws Exception {
        CupParser parser = new CupParser(new LexerCup(new StringReader(input)));
        try {
            parser.parse();
            return true;
        } catch (Exception ex) {
            Symbol sym = parser.getOprSymbol();
            String parserOutput = "There is a syntax error on line: " + (sym.right + 1) + " column: " + (sym.left + 1) + ", value: \"" + sym.value + "\"";
            throw new Exception(parserOutput);
        }
    }
}
