package oprecon;

/*
  UNIBE
  Grupo #7
  Teoria de compiladores.

  It's not that we're so smart, it's just that we stay with problems longer.
*/

import static oprecon.Tokens.*;
%%
%class Lexer
%type Tokens

D=[0-9]
SEPARATOR=[\ \t\r\n]

%{
public String TokenType;
%}
%%

"+" {return ADD;}
"-" {return MINUS;}
"*" {return TIMES;}
"/" {return DIVIDE;}
"^" {return POWER;}
"(" {return LEFT_P;}
")" {return RIGHT_P;}
{SEPARATOR} {TokenType=yytext(); return SEPARATOR;}
{D}+{SEPARATOR}* {TokenType=yytext(); return NUMBER;}
. { return UNEXPECTED; }
