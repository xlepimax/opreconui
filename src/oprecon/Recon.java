/*
  UNIBE
  Grupo #7
  Teoria de compiladores.

  It's not that we're so smart, it's just that we stay with problems longer.
*/

package oprecon;
import java.util.List;
import java.util.ArrayList;
import java.io.StringReader;
import java_cup.runtime.Symbol;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class Recon
{
	static String input = "";
	static String INPUT_PROP = "input";
	static String MESSAGE_PROP = "message";
	static String TOKENS_PROP = "tokens";
	static String ISEXPRESIONVALID_PROP = "isExpressionValid";
	static boolean isExpressionValid = false;
	static JsonObject rootObject = new JsonObject();
	static JsonArray tokensObject = new JsonArray();
	static List<Tokens> tokens = new ArrayList<Tokens>();

	public static void writeExpresionValidity(boolean validity) {
		rootObject.addProperty(ISEXPRESIONVALID_PROP, validity);
	}

	public static void writeRootObject(boolean validity, String message) {
		rootObject.add(TOKENS_PROP, tokensObject);
		rootObject.addProperty(MESSAGE_PROP, message);
		rootObject.addProperty(INPUT_PROP, input);
		writeExpresionValidity(validity);
	}

	public static void main(String[] args) throws Exception
	{
		try
		{
			input = "(4/10)-20*10*(1)^2*33";

			for(int i=0;i<input.length();i++)
			{
				Lexer lexer = new Lexer(new StringReader(String.valueOf(input.charAt(i))));
				Tokens token = lexer.yylex();
				tokensObject.add(token.toString());
				isExpressionValid = !(token == Tokens.UNEXPECTED);
			}

			CupParser parser = new CupParser(new LexerCup(new StringReader(input)));

			try {
				parser.parse();
				writeRootObject(isExpressionValid, "Everything seems to be correct!");
				System.out.println(rootObject.toString());
			} catch (Exception ex) {
				Symbol sym = parser.getOprSymbol();
				String parserOutput = "There is a syntax error on line: " + (sym.right + 1) + " column: " + (sym.left + 1) + ", value: \"" + sym.value + "\"";
				writeRootObject(false, parserOutput);
				System.out.println(rootObject.toString());
			}
		}
		catch(Exception ex)
		{
			throw ex;
		}
	}
}

