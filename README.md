# Grupo 7. #
# Teoría de compiladores. #

## The best group ever.
### It's not that we're so smart, it's just that we stay with problems longer.

### What is this repository for? ###

* Java application to parse and analyze mathematical expressions.
* Version 1

### How do I get set up? ###

To generate needed files run `oprecon.CodeGenerator` file. 
To print out a JSON string with the analysis of your input run `oprecon.Recon`
To set an input open `Recon` file and put your expression into `input` `String` variable.

### Examples you can try for valid expressions

1. `10+7+35+200+1500+07`
2. `545-43^3*100+89+05789/10^4`
3. `((545-43)^3*1001+89+578990)/10^4`
4. `543*4/34*34+100-5*3`

## Examples you can try for invalid expressions

1. `*34/6+55-456+5+5`
2. `..78*36.7/89+44/4`
3. `)78/78+(45/4)-45*6`
4. `(78/78+(45/4)-45*6`

### Collaborators ###

Leonel Pichardo Martínez
18-0743
lpichardomartinez@gmail.com

Andry Guerrero Yberie
18-0742
andrygtec@gmail.com

Michael Cedano
19-0891
Michael.cedano88@gmail.com

Hiram Arnaud 
19-0628
hiram_arnaud@hotmail.com

Luis Enmanuel Carpio
16-0631
ecarpio16@gmail.com

Ángel Beltre 
16-0904 
angelbciprian@gmail.com